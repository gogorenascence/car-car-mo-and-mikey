import React, { useState, useEffect } from 'react'

export default function ManufacturerForm() {
  const [name, setName] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
    };
    const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturersUrl, fetchConfig);

    if (response.ok) {
      setName("");
    }
  };

  const nameChange = (event) => {
    setName(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new manufacturer</h1>
          <form
            onSubmit={handleSubmit}
            id="create-manufacturer-form"
          >
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={nameChange}
                value={name}
                placeholder="Manufacturer name"
                required
                type="text"
                name="name"
                id="name"
              />
              <label htmlFor="name">Manufacturer Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
