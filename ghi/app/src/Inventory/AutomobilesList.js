import React, { useState, useEffect } from 'react'

export default function AutomobilesList() {
  const [autos, setAutos] = useState([])

  useEffect(() => {
    const getAutomobileData = async () => {
      const autoResponse = await fetch("http://localhost:8100/api/automobiles/");
      const autoData = await autoResponse.json();
      setAutos(autoData.autos);
    };

    getAutomobileData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Automobiles</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">VIN</th>
              <th scope="col">Color</th>
              <th scope="col">Year</th>
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {autos.map((auto) => {
              return (
                <tr key={auto.vin}>
                  <td>{auto.vin}</td>
                  <td>{auto.color}</td>
                  <td>{auto.year}</td>
                  <td>{auto.model.name}</td>
                  <td>{auto.model.manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
