import React, { useState, useEffect } from 'react'

export default function TechnicianForm() {
  const [name, setName] = useState("");
  const [employee_number, setEmployeeNumber] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
      employee_number,
    };
    const TechniciansUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(TechniciansUrl, fetchConfig);

    if (response.ok) {
      setName("");
      setEmployeeNumber("");
    }
  };

  const nameChange = (event) => {
    setName(event.target.value);
  };

  const employeeNumberChange = (event) => {
    setEmployeeNumber(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new technician</h1>
          <form
            onSubmit={handleSubmit}
            id="create-technician-form"
          >
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={nameChange}
                value={name}
                placeholder="Technician name"
                required
                type="text"
                name="name"
                id="name"
              />
              <label htmlFor="name">Technician Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                onChange={employeeNumberChange}
                value={employee_number}
                placeholder="employee number"
                required
                type="number"
                name="employee_number"
                id="employee_number"
              />
              <label htmlFor="name">Employee Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
