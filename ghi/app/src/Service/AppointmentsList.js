import React, { useState, useEffect } from 'react'

export default function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  async function fetchData() {
      const response = await fetch("http://localhost:8080/api/appointments/");
          const data = await response.json();
          setAppointments(data.appointments)
  }

  const removeAppointment = async (id) => {
      const response = await fetch(`http://localhost:8080/api/appointment/${id}`, {
          method: 'DELETE',
      });
      const data = await response.json();
      setAppointments(data.appointments)
      window.location.reload();
  }
  const finishAppointment = async (id) => {
      const response = await fetch(`http://localhost:8080/api/appointment/${id}`, {
          method: 'PUT',
      });
      const data = await response.json();
      setAppointments(data.appointments)
  }

  useEffect(() => { fetchData() }, []);

  return (
      <div>
          <h1>Current Services</h1>
          <table className="table table-striped">
              <thead>
                  <tr>
                      <th>VIN</th>
                      <th>Customer Name</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Reason</th>
                      <th>VIP Status</th>
                      <th>Technician</th>
                      <th>Status</th>
                  </tr>
              </thead>
              <tbody>
                  {appointments?.map((appointment) => {
                      return (
                          <tr key={appointment.id}>
                              <td>{appointment.vin}</td>
                              <td>{appointment.customer_name}</td>
                              <td>{(appointment.date)}</td>
                              <td>{(appointment.time)}</td>
                              <td>{(appointment.reason)}</td>
                              <td>{appointment.vip_status.toString()}</td>
                              <td>{appointment.technician.name}</td>
                              <td><button type = "radio" className='btn btn-danger options-outlined' onClick={() => removeAppointment(appointment.id)}>Cancel</button></td>
                              <td><button type = "radio" className='btn btn-success options-outlined' onClick={() => finishAppointment(appointment.id)}>Finish</button></td>
                          </tr>
                      )
                  })}
              </tbody>
          </table>
      </div>
  )
}
