import React from 'react';

class CreateSalesRep extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            employee_name: '',
            employee_id: '',
        };
        this.handleSalesRepChange = this.handleSalesRepChange.bind(this);
        this.handleEmployeeIDChange = this.handleEmployeeIDChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        console.log(this)


        const customerUrl = 'http://localhost:8090/api/salesreps/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newemployee_name = await response.json();
            console.log(newemployee_name)
            const cleared = {
                employee_name: '',
                employee_id: '',
            }
            this.setState(cleared);
        }
    }

    handleSalesRepChange(event) {
        const value = event.target.value;
        this.setState({ employee_name: value })
    }

    handleEmployeeIDChange(event) {
        const value = event.target.value;
        this.setState({ employee_id: value })
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Employee Form</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleSalesRepChange} value={this.state.employee_name} placeholder="Employee Name" required type="text" name="employee_name" id="employee_name" className="form-control" />
                                <label htmlFor="semployee_name">Employee Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmployeeIDChange} value={this.state.employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                                <label htmlFor="employee_id">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateSalesRep;
