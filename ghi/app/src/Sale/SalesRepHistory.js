import React, { useState, useEffect } from 'react';

export default function SalesRepHistory() {
    const [list, setList] = useState();

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/autosales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setList(data.autosales)
        }
    }

    const [query, setQuery] = useState('')

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <>
            <div className="container">
                <div className="row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="input-group">
                                <input onChange={event => setQuery(event.target.value)} className="form-control" placeholder="Search a SalesRep" type="text" />
                            </div>
                        </div>
                    </form>
                </div>
                <h1>Sales History </h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Rep</th>
                            <th>Employee ID</th>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list?.filter(autosale =>
                        autosale.salesrep.employee_name.includes(query))
                            .map(autosale => {
                                return (
                                    <tr key={autosale.id}>
                                        <td>{autosale.salesrep.employee_name}</td>
                                        <td>{autosale.salesrep.employee_id}</td>
                                        <td>{autosale.customer.customer_name}</td>
                                        <td>{autosale.vehicle.vin}</td>
                                        <td>{autosale.price}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        </>
    )
}
