import React from 'react';

class AddAutoSale extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      price: '',
      vehicle: '',
      automobiles: [],
      salesrep: '',
      salesreps: [],
      customer: '',
      customers: [],
    };
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
    this.handleSalesRepChange = this.handleSalesRepChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.salesreps;
    delete data.automobiles;
    delete data.customers;


    const autosaleUrl = 'http://localhost:8090/api/autosales/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(data)
    const response = await fetch(autosaleUrl, fetchConfig);
    if (response.ok) {

      const newAutosale = await response.json();
      const cleared = {
        price: '',
        automobile: '',
        salesrep: '',
        customer: '',
      }
      this.setState(cleared);
    }
  }

  handlePriceChange(event) {
    const value = event.target.value;
    this.setState({ price: value })
  }

  handleAutomobileChange(event) {
    const value = event.target.value;
    this.setState({ vehicle: value })
  }

  handleSalesRepChange(event) {
    const value = event.target.value;
    this.setState({ salesrep: value })
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value })
  }

  async componentDidMount() {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const automobileResponse = await fetch(automobileUrl);
    if (automobileResponse.ok) {
      const automobileData = await automobileResponse.json();
      this.setState({ automobiles: automobileData.autos });
    }


    const salesrepUrl = 'http://localhost:8090/api/salesreps/';
    const salesrepResponse = await fetch(salesrepUrl);
    if (salesrepResponse.ok) {
      const salesrepData = await salesrepResponse.json();
      this.setState({ salesreps: salesrepData.salesreps })
    }

    const customerUrl = 'http://localhost:8090/api/customers/';
    const customerResponse = await fetch(customerUrl);
    if (customerResponse.ok) {
      const customerData = await customerResponse.json();
      this.setState({ customers: customerData.customers })
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new autosale</h1>
            <form onSubmit={this.handleSubmit} id="Create-Autosale-Form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePriceChange} value={this.state.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
                <select value={this.state.automobile} onChange={this.handleAutomobileChange} required id="automobile"
                  className="form-select" name="automobile" >
                  <option value="">Choose vehicle</option>
                  {this.state.automobiles.map(automobile => {
                    return (
                      <option key={automobile.vin} value={automobile.vin}>
                        {automobile.model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={this.state.salesrep} onChange={this.handleSalesRepChange} required id="salesrep"
                  className="form-select" name="salesrep" >
                  <option value="">Choose Sales Rep</option>
                  {this.state.salesreps.map(salesrep => {
                    return (
                      <option key={salesrep.id} value={salesrep.employee_number}>
                        {salesrep.employee_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={this.state.customer} onChange={this.handleCustomerChange} required id="customer"
                  className="form-select" name="customer" >
                  <option value="">Choose customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.customer_name}>
                        {customer.customer_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default AddAutoSale;
