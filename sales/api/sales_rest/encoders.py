from .models import AutoSale, AutomobileVO, Customer, SalesRep
from common.json import ModelEncoder


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]


class SalesRepListEncoder(ModelEncoder):
    model = SalesRep
    properties = ["id", "employee_name", "employee_id"]


class SalesRepDetailEncoder(ModelEncoder):
    model = SalesRep
    properties = ["id", "employee_name", "employee_id"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "customer_name", "address", "phone_number"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "customer_name", "address", "phone_number"]


class AutoSaleListEncoder(ModelEncoder):
    model = AutoSale
    properties = [
        "id",
        "price",
        "vehicle",
        "salesrep",  # employee_name
        "customer"
    ]

    encoders = {
        "vehicle": AutomobileVODetailEncoder(),
        "salesrep": SalesRepListEncoder(),  # employee_name
        "customer": CustomerListEncoder(),
    }

class AutoSaleDetailEncoder(ModelEncoder):
    model = AutoSale
    properties = [
        "id",
        "price",
        "vehicle",
        "salesrep",  
        "customer"
    ]

    encoders = {
        "vehicle": AutomobileVODetailEncoder(),
        "salesrep": SalesRepListEncoder(),  # employee_name
        "customer": CustomerListEncoder(),
    }
