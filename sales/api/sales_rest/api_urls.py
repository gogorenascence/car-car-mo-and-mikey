from django.urls import path
from .views import (
    api_sales,
    api_customers,
    api_salesreps,
)

urlpatterns = [
    path(
        "autosales/",
        api_sales,
        name="api_sales",
    ),

    path(
        "customers/",
        api_customers,
        name="api_customers",
    ),
    path(
        "salesreps/",
        api_salesreps,
        name="api_salesreps",
    ),
]
