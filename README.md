# CarCar

Team:

* Mikey - Sales microservice
* Mo Khan - Service microservice

## Design

## Service microservice

The Service microservice consists of three models:
----------------------------------------------------
    1. Appointment:
        This model manages information about appointments, such as the customer's name, the date and time the appointment was made, the reason for the appointment, the technician assigned to the appointment, the status of whether the appointment is finished or not, and the vehicle identification number (VIN) of the vehicle being serviced. The model also tracks whether the appointment has been completed and whether the vehicle associated with the VIN has been sold by the dealership

    2. Technician
        This model manages information about a technician's name and their employee number.To ensure that each technician has an unique employee number, the employee_number attribute has unique=True.

    3. AutomobileVO
        This Value Object model manages the information about a specific automobile by looking at its vin and import_href from the Automobile model in the Inventory microservice.

## Sales microservice
1.Create models
2.Create api_views
3.Route views to sales_project file
4.Create react frontend forms
5.Create proper navs for links on website
6.Create proper routes for forms in app.js
